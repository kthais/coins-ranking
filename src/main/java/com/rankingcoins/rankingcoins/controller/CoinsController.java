package com.rankingcoins.rankingcoins.controller;

import com.rankingcoins.rankingcoins.request.Coins;
import com.rankingcoins.rankingcoins.service.CoinsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/coins")
public class CoinsController {

    @Autowired
    private CoinsService coinsService;


    @GetMapping
    public List<Coins> getCoinsList(){
        return coinsService.getCoinsList();
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<?>getCoinById(@PathVariable String uuid) throws Exception {

        return ResponseEntity.ok().body(coinsService.getCoinsById(uuid));
    }

    @GetMapping("/timePeriodAndId")
    ResponseEntity<?>getCoinByIdOrPeriod(@RequestParam String uuid, @RequestParam String timePeriod) throws Exception {
        return ResponseEntity.ok().body(coinsService.getCoinsByTimePeriod(uuid,timePeriod));
    }

    @GetMapping("/priceBitCoin/{uuid}")
    ResponseEntity<?>getCoinByPriceBitCoin(@PathVariable String uuid, @RequestParam String timestamp) throws Exception {
        return ResponseEntity.ok().body(coinsService.getCoinsByPrice(uuid,timestamp));
    }

    @GetMapping("/saveBitcoin/{uuid}")
    ResponseEntity<?>getCoinBySaveBitCoin(@PathVariable String uuid) throws Exception {
        return ResponseEntity.ok().body(coinsService.saveCoinByUUID(uuid));
    }
}
