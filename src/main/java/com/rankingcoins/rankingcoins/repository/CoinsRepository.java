package com.rankingcoins.rankingcoins.repository;

import com.rankingcoins.rankingcoins.model.CoinModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoinsRepository extends JpaRepository<CoinModel, String> {

    boolean existsCoinModelByUuidApi(String uuid);
}
