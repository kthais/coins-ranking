package com.rankingcoins.rankingcoins.response;


import com.rankingcoins.rankingcoins.data.CoinsData;

public class CoinsResponse {
    private CoinsData data;

    public CoinsData getData() {
        return data;
    }

    public void setData(CoinsData data) {
        this.data = data;
    }
}