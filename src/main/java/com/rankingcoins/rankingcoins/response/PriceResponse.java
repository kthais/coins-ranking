package com.rankingcoins.rankingcoins.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.rankingcoins.rankingcoins.data.PriceData;

public class PriceResponse {

    private PriceData data;

    private String status;

    public PriceData getData() {
        return data;
    }

    public void setData(PriceData data) {
        this.data = data;
    }
    @JsonProperty("status")
    public String getStatus() {
        return status;
    }
}
