package com.rankingcoins.rankingcoins.service;

import com.rankingcoins.rankingcoins.model.CoinModel;
import com.rankingcoins.rankingcoins.repository.CoinsRepository;
import com.rankingcoins.rankingcoins.request.Coins;
import com.rankingcoins.rankingcoins.response.CoinsResponse;
import com.rankingcoins.rankingcoins.response.PriceResponse;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
public class CoinsService {

    private final RestTemplate restTemplate;
    private final CoinsRepository coinsRepository;

    private final String API_URL = "https://api.coinranking.com/v2/coins";
    private final String URL_UUID = "https://api.coinranking.com/v2/coin";


    public CoinsService(RestTemplate restTemplate,CoinsRepository coinsRepository) {
        this.restTemplate = restTemplate;
        this.coinsRepository = coinsRepository;
    }

    public List<Coins> getCoinsList() {
        var responseEntity = restTemplate.exchange(
                API_URL,
                HttpMethod.GET,
                null,
                CoinsResponse.class
        );

        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            CoinsResponse coinsResponse = responseEntity.getBody();
            return coinsResponse.getData().getCoins();
        }

        return Collections.emptyList();
    }

    public ResponseEntity<?> getCoinsByTimePeriod(String uuid, String timePeriod) {
        if (uuid == null) {
            return ResponseEntity.badRequest().body("Para a pesquisa atual o uuid deve ser informado obrigatoriamente.");
        }

        var responseEntity = restTemplate.exchange(
                URL_UUID.concat("/").concat(uuid).concat("?").concat(timePeriod),
                HttpMethod.GET,
                null,
                CoinsResponse.class
        );

        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            CoinsResponse coinsResponse = responseEntity.getBody();
            return ResponseEntity.ok().body(coinsResponse);

        } else if (responseEntity.getStatusCode() == HttpStatus.NOT_FOUND) {
            return ResponseEntity.notFound().build();

        } else if (responseEntity.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR) {
            return ResponseEntity.internalServerError().build();
        }

        return ResponseEntity.badRequest().body("Nao foi possivel realizar a consulta por periodos de bitcoins ");
    }


    public ResponseEntity<?> getCoinsByPrice(String uuid, String timestamp) {
        if (uuid == null) {
            return ResponseEntity.badRequest().body("Para a pesquisa atual o uuid deve ser informado obrigatoriamente.");
        }

        var responseEntity = restTemplate.exchange(
                URL_UUID.concat("/").concat(uuid).concat("/price").concat("?").concat(timestamp),
                HttpMethod.GET,
                null,
                PriceResponse.class
        );

        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            PriceResponse priceResponse = responseEntity.getBody();
            return ResponseEntity.ok().body(priceResponse);

        } else if (responseEntity.getStatusCode() == HttpStatus.NOT_FOUND) {
            return ResponseEntity.notFound().build();

        } else if (responseEntity.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR) {
            return ResponseEntity.internalServerError().build();
        }

        return ResponseEntity.badRequest().body("Nao foi possivel realizar a consulta por precos de bitcoins ");
    }

    public ResponseEntity<CoinsResponse> getCoinsById(String uuid) throws Exception {

        var responseEntity = restTemplate.exchange(
                URL_UUID.concat("/").concat(uuid),
                HttpMethod.GET,
                null,
                CoinsResponse.class
        );

        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            CoinsResponse coinsResponse = responseEntity.getBody();

            if (coinsResponse == null) {
                throw new Exception("Não existe a moeda em nossa base de dados.");
            }
        }

        return ResponseEntity.status(HttpStatus.OK).body(responseEntity.getBody());

    }

    public ResponseEntity<?> saveCoinByUUID(String uuid) throws Exception {
        var searchCoinsByApi = getCoinsById(uuid);

        if (coinsRepository.existsCoinModelByUuidApi(uuid)) {
            return ResponseEntity.badRequest().body("Atualmente já existe uma moeda cadastrada com o uuid pesquisado.");
        }

        if (searchCoinsByApi.getStatusCode() == HttpStatus.OK) {
            CoinModel coins = new CoinModel();

            coins.setUuid(Objects.requireNonNull(searchCoinsByApi.getBody()).getData().getCoin().getUuid());
            coins.setUuidApi(uuid);
            coins.setName(Objects.requireNonNull(searchCoinsByApi.getBody().getData().getCoin().getName()));

            coinsRepository.save(coins);

        } else if (searchCoinsByApi.getStatusCode() == HttpStatus.NOT_FOUND) {
            return ResponseEntity.notFound().build();

        } else if (searchCoinsByApi.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR) {
            return ResponseEntity.internalServerError().build();

        } else if (searchCoinsByApi.getStatusCode() == HttpStatus.BAD_REQUEST) {
            return ResponseEntity.badRequest().body("Ocorreu um erro ao tentar salvar o bitcoin pesquisado...");
        }

        return searchCoinsByApi;
    }
}