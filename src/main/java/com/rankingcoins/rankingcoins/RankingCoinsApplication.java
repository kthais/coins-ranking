package com.rankingcoins.rankingcoins;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RankingCoinsApplication {

	public static void main(String[] args) {
		SpringApplication.run(RankingCoinsApplication.class, args);
	}

}
