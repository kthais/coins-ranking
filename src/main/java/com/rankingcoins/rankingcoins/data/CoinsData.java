package com.rankingcoins.rankingcoins.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.rankingcoins.rankingcoins.request.Coins;
import java.util.List;

public class CoinsData {
    private List<Coins> coins;
    private Coins coin;

    public Coins getCoin() {
        return coin;
    }

    public Coins setCoin(Coins coin) {
        this.coin = coin;
        return coin;
    }

    @JsonProperty("coins")
    public List<Coins> getCoins() {
        return coins;
    }

    @JsonProperty("coins")
    public void setCoins(List<Coins> coins) {
        this.coins = coins;
    }
}