package com.rankingcoins.rankingcoins.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PriceData {
    private String price;
    private String timestamp;

    @JsonProperty("price")
    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
    @JsonProperty("timestamp")
    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
